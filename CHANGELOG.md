
# Changelog
All notable changes to this project will be documented in this file.

## [0.2.1] - 2023-01-11
### Changed
- `strum-esb` dependency version to `~> 0.4` by [@valeriia.kolisnyk].

## [0.2.0] - 2022-03-27
### Changed
  - init step always start new pipeline
### Fixed
  - storage per thread

## [0.1.4] - 2021-01-11
### Changed
- `strum-esb` dependency version to `~> 0.2` by [@valeriia.kolisnyk].
- pipeline name format to snake case by [@valeriia.kolisnyk].

## [0.1.2] - 2021-07-01
### Added
- Redis connection using env var or config by [@serhiy.nazarov].

## [0.1.1] - 2021-06-02
### Fixed
- custom handlers for resource name with `-` by [@anton.klyzhka].

### Changed
- `strum-esb` dependency version to `~> 0.1.1` by [@anton.klyzhka].

# frozen_string_literal: true

require "strum/pipeline/version"
require "redis/objects"
require 'connection_pool'
require "strum/esb"
require "strum/pipeline/storage"
require "dry/configurable"
require "securerandom"

module Strum
  module Pipeline # rubocop:disable Style/Documentation
    extend Dry::Configurable

    setting :ttl, 15 * 60 # default ttl is 15 mins
    setting :redis_connection, {
      host: ENV.fetch("PIPELINE_REDIS_HOST", "localhost"),
      port: ENV.fetch("PIPELINE_REDIS_PORT", "6379"),
      db: ENV.fetch("PIPELINE_REDIS_DB", "0")
    }
    # setting :redis_connection_pool

    Strum::Esb.config.after_fork_hooks << proc do
      Redis::Objects.redis = ConnectionPool.new(size: 5, timeout: 5) do
        Redis.new(Strum::Pipeline.config.redis_connection)
      end
    end

    class Error < StandardError; end

    # Classes = []

    def self.included(base)
      base.class_eval do
        include Strum::Esb::Handler

        attr_accessor :handler_bind

        def after_headers_hook(bind)
          @handler_bind = bind
          setup_pipeline
        end

        def setup_pipeline
          Thread.current[:pipeline] ||= self.class.pipeline_name
          Thread.current[:pipeline_id] = SecureRandom.hex(10) if init_step?
        end

        def init_step?
          if eval("action", handler_bind)
            self.class.inits.include?(["action", eval("action", handler_bind), eval("resource", handler_bind)].join("-"))
          elsif eval("event", handler_bind)
            self.class.inits.include?(["event", eval("resource", handler_bind), eval("event", handler_bind), eval("state", handler_bind)].join("-"))
          elsif eval("info", handler_bind)
            self.class.inits.include?(["info", eval("info", handler_bind)].join("-"))
          elsif eval("notice", handler_bind)
            self.class.inits.include?(["notice", eval("resource", handler_bind), eval("notice", handler_bind)].join("-"))
          end
        end
      end

      base.extend ClassMethods
    end

    # class methods
    module ClassMethods
      def ttl(interval)
        Strum::Pipeline.config.ttl = interval
      end

      def inits
        @inits ||= []
      end
      
      def init(message_type, message_binding, handler = nil)
        bind_to(pipeline_name, message_type, message_binding, handler)
        inits << handler_key(message_type, message_binding)
      end

      def step(message_type, message_binding, handler = nil)
        bind_to(pipeline_name, message_type, message_binding, handler) do |bindings|
          bindings[:pipeline] ||= {}
          bindings[:pipeline][:name] = pipeline_name
          bindings[:pipeline][message_type] ||= []
          bindings[:pipeline][message_type] << message_binding
        end
      end

      def pipeline_name
        word = name.dup
        word.gsub!(/::/, "_")
        word.gsub!(/([A-Z]+)([A-Z][a-z])/, '\1_\2')
        word.gsub!(/([a-z\d])([A-Z])/, '\1_\2')
        word.tr!("-", "_")
        word.downcase!
        word
      end
    end

    def storage
      Thread.current[:storage] ||= Strum::Pipeline::Storage.new(pipeline_key, Strum::Pipeline.config.ttl)
    end

    def pipeline_key
      [Thread.current[:pipeline], Thread.current[:pipeline_id]].join("-")
    end
  end
end

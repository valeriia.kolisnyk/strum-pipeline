# frozen_string_literal: true

require "redis/objects"
require "singleton"
require "dry/inflector"

# One storage per One Pipiline
# One Storage type per each Pipeline type

module Strum
  module Pipeline
    # Class to w/r to store
    class Storage
      attr_accessor :pipeline, :ttl

      def initialize(pipeline, ttl)
        @pipeline = pipeline
        @ttl = ttl
      end

      def method_missing(method)
        that = self
        Class.new do
          define_singleton_method :[] do |key|
            Redis.const_get(Dry::Inflector.new.camelize(method)).new("#{that.pipeline}::#{key}", expiration: that.ttl)
          end
        end

      rescue StandardError
        super
      end

      def respond_to_missing?(method)
        Redis.const_get(Dry::Inflector.new.camelize(method))
      rescue StandardError
        super
      end

      def [](key)
        Redis::Value.new("#{pipeline}::#{key}").value
      end

      def []=(key, value)
        Redis::Value.new("#{pipeline}::#{key}", expiration: ttl).value = value
      end
    end
  end
end
